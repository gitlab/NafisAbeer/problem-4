package edu.bu.ec504.spr24.brain;

import edu.bu.ec504.spr24.brain.Board.Pos;
import javax.swing.*;

/**
 * A smarter brain, for you to produce.
 */
public class SmarterBrain extends Brain {
    public SmarterBrain() {
        super();
    }

    @Override
    public String myName() {
        return null;
    }

    @Override
    Pos nextMove() {
        int bestScore = 0;
        Pos bestMove = new Pos(0, 0);
        Board currBoard = new Board();
        
        // Initialize the board state
        for (int x = 0; x < myGUI.boardWidth(); x++) {
            for (int y = 0; y < myGUI.boardHeight(); y++) {
                currBoard.modify(x, y, myGUI.colorAt(x, myGUI.boardHeight() - y - 1));
            }
        }
        
        // Evaluate each possible move
        for (int x = 0; x < currBoard.columns(); x++) {
            for (int y = 0; y < currBoard.rows(x); y++) {
                if (currBoard.getAt(x, y) != CircleColor.NONE) {
                    Board testBoard = new Board(currBoard);
                    int score = testBoard.clickNode(x, y);
                    
                    // Prefer moves that lead to a better future board state
                    int futurePotential = evaluateFuturePotential(testBoard);
                    int totalScore = score + futurePotential;
                    
                    if (totalScore > bestScore) {
                        bestScore = totalScore;
                        bestMove = new Pos(x, myGUI.boardHeight() - y - 1);
                    }
                }
            }
        }
        
        if (bestScore == 0) { // No move found
            JOptionPane.showMessageDialog(null, "No viable move left!");
            return null;
        }
        
        return bestMove;
    }

    private int evaluateFuturePotential(Board board) {
    int potentialScore = 0;
    int[][] adjacencyMatrix = new int[board.columns()][board.rows(0)]; // Assume uniform row count for simplicity
    
    // Initialize adjacency matrix
    for (int i = 0; i < board.columns(); i++) {
        for (int j = 0; j < board.rows(i); j++) {
            adjacencyMatrix[i][j] = 0;
        }
    }
    
    // Calculate adjacency scores for each circle
    for (int x = 0; x < board.columns(); x++) {
        for (int y = 0; y < board.rows(x); y++) {
            CircleColor currentColor = board.getAt(x, y);
            if (currentColor != CircleColor.NONE) {
                // Check adjacent circles and increase score for each of the same color
                int adjacentSameColor = 0;
                if (x > 0 && board.getAt(x - 1, y) == currentColor) adjacentSameColor++;
                if (x < board.columns() - 1 && board.getAt(x + 1, y) == currentColor) adjacentSameColor++;
                if (y > 0 && board.getAt(x, y - 1) == currentColor) adjacentSameColor++;
                if (y < board.rows(x) - 1 && board.getAt(x, y + 1) == currentColor) adjacentSameColor++;
                
                adjacencyMatrix[x][y] = adjacentSameColor;
            }
        }
    }
    
    // Sum up potential scores based on adjacency
    for (int x = 0; x < adjacencyMatrix.length; x++) {
        for (int y = 0; y < adjacencyMatrix[x].length; y++) {
            // This is a simple heuristic: more adjacent circles of the same color could mean higher potential scores.
            potentialScore += adjacencyMatrix[x][y];
        }
    }
    
    return potentialScore;
}
 

}
